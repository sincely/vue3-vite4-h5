import Components from 'unplugin-vue-components/vite' // 自动导入组件,不需要手动导入
import AutoImport from 'unplugin-auto-import/vite' // 自动导入组件,不需要手动导入
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'
import { VantResolver } from '@vant/auto-import-resolver'
export default function createVitePlugins() {
  return [
    Icons({ autoInstall: true, compiler: 'vue3' }),
    Components({
      dirs: ['src/components'], // 指定组件位置，默认是src/components
      resolvers: [VantResolver(), IconsResolver()],
      extensions: ['vue'], // 指定扩展名，默认是.vue
      dts: false // 生成.d.ts文件
    }),
    AutoImport({
      imports: ['vue', 'vue-router', 'pinia'],
      include: [/\.[tj]sx?$/, /\.vue$/],
      dts: false,
      resolvers: [], // 第三方ui库的按需引入，比如element-plus，vant等
      // 根据项目情况配置eslintrc，默认是不开启的
      // 下面两个是其他配置，默认即可
      // 输出一份json文件，默认输出路径为./.eslintrc-auto-import.json
      eslintrc: {
        enabled: false, // @default false 是否开启 eslint 检测
        filepath: './.eslintrc-auto-import.json', // @default './.eslintrc-auto-import.json'
        globalsPropValue: true // @default true 可设置 boolean | 'readonly' | 'readable' | 'writable' | 'writeable'
      }
    })
  ]
}
