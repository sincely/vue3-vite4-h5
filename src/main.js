import { createApp } from 'vue'
import { store } from './store'
import 'normalize.css/normalize.css'
import '@/plugins'
import './styles/index.less'
import 'virtual:svg-icons-register'

import App from './App.vue'
import router from './router'

const app = createApp(App)
app.use(store)
app.use(router)
app.mount('#app')
