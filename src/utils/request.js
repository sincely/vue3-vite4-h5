import axios from 'axios'
const service = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL, // api的base_url
  withCredentials: true, // 跨域请求时发送cookies
  timeout: 5000 // 请求超时时间
})

// request拦截器 request interceptor
service.interceptors.request.use(
  (config) => {
    // if (store.getters.token) {
    //   config.headers['Authorization'] = 'Bearer' // 让每个请求携带自定义token请根据实际情况自行修改
    // }
    return config
  },
  (error) => {
    // 请求错误处理
    Promise.reject(error)
  }
)
// respone拦截器
service.interceptors.response.use(
  (response) => {
    const res = response.data
    if (res.status && res.status !== 200) {
      // 登录超时,重新登录
      if (res.status === 401) {
        console.log(1111111)
      }
      return Promise.reject(res || 'error')
    }
    return Promise.resolve(res.data)
  },
  (error) => {
    if (error && error.response) {
      switch (error.response.status) {
        case 400:
          error.message = '错误请求'
          break
        case 401:
          error.message = '未授权，请重新登录'
          break
        case 403:
          error.message = '拒绝访问'
          break
        case 404:
          error.message = '请求错误,未找到该资源'
          break
        case 405:
          error.message = '请求方法未允许'
          break
        case 408:
          error.message = '请求超时'
          break
        case 500:
          error.message = '服务器端出错'
          break
        case 501:
          error.message = '网络未实现'
          break
        case 502:
          error.message = '网络错误'
          break
        case 503:
          error.message = '服务不可用'
          break
        case 504:
          error.message = '网络超时'
          break
        case 505:
          error.message = 'http版本不支持该请求'
          break
        default:
          error.message = `未知错误${error.response.status}`
      }
    } else {
      error.message = '连接到服务器失败'
    }
    return Promise.reject(error)
  }
)

export default service
